package org.openiam.am.srvc.service;

import org.apache.cxf.service.model.BindingMessageInfo;
import org.openiam.idm.srvc.user.domain.UserAttributeEntity;
import org.openiam.idm.srvc.user.domain.UserEntity;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class AbstractAuthResourceAttributeMapper implements AuthResourceAttributeMapper {
    private Map<String, UserAttributeEntity> userAttributeMap;
    private ApplicationContext applicationContext;
    private UserEntity user;

    @Override
    public void init(Map<String, Object> bindingMap){
        this.userAttributeMap = (Map<String, UserAttributeEntity>)bindingMap.get("userAttributeMap");
        this.applicationContext = (ApplicationContext)bindingMap.get("applicationContext");
        this.user = (UserEntity)bindingMap.get("user");
    }
    @Override
    public String mapAttribute(){
        String attributeValue = "";
        System.out.println("Getting attribute with name:" + this.getAttributeName());
        UserAttributeEntity attribute = userAttributeMap.get(this.getAttributeName());
        if(attribute!=null && attribute.getValue()!=null && !attribute.getValue().trim().isEmpty()){
            System.out.println("Attribute found");
            attributeValue = mapValue(attribute.getValue());
        } else if (attribute!=null && attribute.getValue() == null && !attribute.getValues().isEmpty()) {
            System.out.println("Getting attribute list with name:" + this.getAttributeName());
            for (String value : attribute.getValues()) {
                attributeValue += value + "|";
            }
            attributeValue = attributeValue.substring(0, attributeValue.length() - 1);
        }   else{
            System.out.println("Attribute not found");
        }
        return attributeValue;
    }

    @Override
    public List<String> mapAttributeList(){
        List<String> attributeValueList = new ArrayList<>();
        System.out.println("Getting attribute list with name:" + this.getAttributeName());
        UserAttributeEntity attribute = userAttributeMap.get(this.getAttributeName());
        if(attribute!=null && attribute.getValue() == null && !attribute.getValues().isEmpty()){
            System.out.println("Attribute list found");
            attributeValueList = mapValueList(attribute.getValues());
        } else {
            System.out.println("Attribute list not found");
        }
        return attributeValueList;
    }
    
    protected UserEntity getUser() {
    	return this.user;
    }

    protected abstract String mapValue(String value);

    protected abstract String getAttributeName();

    protected abstract List<String> mapValueList(List<String> values);
}
