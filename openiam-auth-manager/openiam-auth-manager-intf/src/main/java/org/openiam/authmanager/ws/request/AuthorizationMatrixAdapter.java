package org.openiam.authmanager.ws.request;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.openiam.authmanager.ws.request.AuthorizationMatrixMap.AuthorizationMatrixEntry;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.*;

public class AuthorizationMatrixAdapter extends XmlAdapter<AuthorizationMatrixMap, Map<String, Set<String>>> {

	@Override
	public Map<String, Set<String>> unmarshal(final AuthorizationMatrixMap v)
			throws Exception {
		final Map<String, Set<String>> retVal = new HashMap<String, Set<String>>();
		if(v != null && CollectionUtils.isNotEmpty(v.getEntries())) {
			for(final AuthorizationMatrixEntry entry : v.getEntries()) {
				retVal.put(entry.getKey(), entry.getValues());
			}
		}
		return retVal;
	}

	@Override
	public AuthorizationMatrixMap marshal(final Map<String, Set<String>> v)
			throws Exception {
		final AuthorizationMatrixMap retVal = new AuthorizationMatrixMap();
		if(v != null && MapUtils.isNotEmpty(v)) {
			final List<AuthorizationMatrixMap.AuthorizationMatrixEntry> entries = new LinkedList<AuthorizationMatrixMap.AuthorizationMatrixEntry>();
			for(final String key : v.keySet()) {
				entries.add(new AuthorizationMatrixEntry(key, v.get(key)));
			}
			retVal.setEntries(entries);
		}
		return retVal;
	}

}
