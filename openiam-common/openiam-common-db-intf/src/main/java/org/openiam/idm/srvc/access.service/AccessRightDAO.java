package org.openiam.idm.srvc.access.service;

import org.openiam.am.srvc.domain.ContentProviderEntity;
import org.openiam.core.dao.BaseDao;
import org.openiam.idm.srvc.access.domain.AccessRightEntity;
import org.openiam.idm.srvc.res.domain.ResourceEntity;

public interface AccessRightDAO extends BaseDao<AccessRightEntity, String> {

}
