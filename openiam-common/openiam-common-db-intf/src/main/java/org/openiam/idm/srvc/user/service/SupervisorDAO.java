package org.openiam.idm.srvc.user.service;

import org.openiam.core.dao.BaseDao;
import org.openiam.idm.srvc.user.domain.SupervisorEntity;
import org.openiam.idm.srvc.user.domain.SupervisorIDEntity;
import org.openiam.idm.srvc.user.dto.Supervisor;

import java.util.List;
import java.util.Set;

public interface SupervisorDAO extends BaseDao<SupervisorEntity, SupervisorIDEntity> {

    public Set<String> getUniqueEmployeeIds();

    public void deleteById(SupervisorIDEntity id);
}