package org.openiam.idm.srvc.org.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.openiam.idm.srvc.user.domain.UserEntity;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by zaporozhec on 7/17/15.
 */
@Embeddable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class OrganizationUserIdEntity implements Serializable {
    @ManyToOne
    private UserEntity user;
    @ManyToOne
    private OrganizationEntity organization;

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }


    public OrganizationEntity getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationEntity organization) {
        this.organization = organization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrganizationUserIdEntity that = (OrganizationUserIdEntity) o;

        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        return organization != null ? organization.equals(that.organization) : that.organization == null;
    }

    @Override
    public int hashCode() {
        int result = user != null ? user.hashCode() : 0;
        result = 31 * result + (organization != null ? organization.hashCode() : 0);
        return result;
    }
}
