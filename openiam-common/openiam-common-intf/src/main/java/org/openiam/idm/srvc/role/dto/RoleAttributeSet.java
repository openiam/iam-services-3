package org.openiam.idm.srvc.role.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "roleAttributeSet", propOrder = {
        "roleAttributeObj"
})
public class RoleAttributeSet {

    protected List<RoleAttributeSet.RoleAttributeObj> roleAttributeObj;

    public List<RoleAttributeSet.RoleAttributeObj> getRoleAttributeObj() {
        if (roleAttributeObj == null) {
            roleAttributeObj = new ArrayList<RoleAttributeSet.RoleAttributeObj>();
        }
        return this.roleAttributeObj;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "roleAttribute"
    })
    public static class RoleAttributeObj {

        protected RoleAttribute roleAttribute;

        /**
         * Gets the value of the roleAttribute property.
         *
         * @return possible object is
         *         {@link RoleAttribute }
         */
        public RoleAttribute getRoleAttribute() {
            return roleAttribute;
        }

        /**
         * Sets the value of the roleAttribute property.
         *
         * @param value allowed object is
         *              {@link RoleAttribute }
         */
        public void setRoleAttribute(RoleAttribute value) {
            this.roleAttribute = value;
        }

    }

}
