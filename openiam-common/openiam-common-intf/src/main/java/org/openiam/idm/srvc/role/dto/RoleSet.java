package org.openiam.idm.srvc.role.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "roleSet", propOrder = {
        "roleObj"
})
public class RoleSet {

    protected List<RoleSet.RoleObj> roleObj;

    /**
     * Objects of the following type(s) are allowed in the list
     * {@link RoleSet.RoleObj }
     **/
    public List<RoleSet.RoleObj> getRoleObj() {
        if (roleObj == null) {
            roleObj = new ArrayList<RoleSet.RoleObj>();
        }
        return this.roleObj;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "role"
    })
    public static class RoleObj {

        protected Role role;

        /**
         * Gets the value of the role property.
         *
         * @return possible object is
         * {@link Role }
         */
        public Role getRole() {
            return role;
        }

        /**
         * Sets the value of the role property.
         *
         * @param value allowed object is
         *              {@link Role }
         */
        public void setRole(Role value) {
            this.role = value;
        }

    }

}
