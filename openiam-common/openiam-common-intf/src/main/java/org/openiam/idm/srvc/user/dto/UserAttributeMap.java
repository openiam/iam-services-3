package org.openiam.idm.srvc.user.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userAttributeMap", propOrder = {
        "userAttributeEntry"
})
public class UserAttributeMap {

    protected List<UserAttributeMap.UserAttributeEntry> userAttributeEntry;

    /**
     * Gets the value of the userAttributeEntry property.
     * Objects of the following type(s) are allowed in the list
     * {@link UserAttributeMap.UserAttributeEntry }
     */
    public List<UserAttributeMap.UserAttributeEntry> getUserAttributeEntry() {
        if (userAttributeEntry == null) {
            userAttributeEntry = new ArrayList<UserAttributeMap.UserAttributeEntry>();
        }
        return this.userAttributeEntry;
    }



    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "userAttribute"
    })
    public static class UserAttributeEntry {

        protected UserAttribute userAttribute;
        @XmlAttribute
        protected String key;

        /**
         * Gets the value of the userAttribute property.
         *
         * @return possible object is
         *         {@link UserAttribute }
         */
        public UserAttribute getUserAttribute() {
            return userAttribute;
        }

        /**
         * Sets the value of the userAttribute property.
         *
         * @param value allowed object is
         *              {@link UserAttribute }
         */
        public void setUserAttribute(UserAttribute value) {
            this.userAttribute = value;
        }

        /**
         * Gets the value of the key property.
         *
         * @return possible object is
         *         {@link String }
         */
        public String getKey() {
            return key;
        }

        /**
         * Sets the value of the key property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setKey(String value) {
            this.key = value;
        }

    }

}
