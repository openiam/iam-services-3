package org.openiam.idm.srvc.user.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userAttributeSet", propOrder = {
        "userAttributeObj"
})
public class UserAttributeSet {

    protected List<UserAttributeSet.UserAttributeObj> userAttributeObj;

    /**
     * Gets the value of the userAttributeObj property.
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userAttributeObj property.
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserAttributeSet.UserAttributeObj }
     */
    public List<UserAttributeSet.UserAttributeObj> getUserAttributeObj() {
        if (userAttributeObj == null) {
            userAttributeObj = new ArrayList<UserAttributeSet.UserAttributeObj>();
        }
        return this.userAttributeObj;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "userAttribute"
    })
    public static class UserAttributeObj {

        protected UserAttribute userAttribute;

        /**
         * Gets the value of the userAttribute property.
         *
         * @return possible object is
         * {@link UserAttribute }
         */
        public UserAttribute getUserAttribute() {
            return userAttribute;
        }

        /**
         * Sets the value of the userAttribute property.
         *
         * @param value allowed object is
         *              {@link UserAttribute }
         */
        public void setUserAttribute(UserAttribute value) {
            this.userAttribute = value;
        }

    }

}
