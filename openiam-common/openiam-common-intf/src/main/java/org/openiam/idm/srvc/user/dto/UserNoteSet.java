package org.openiam.idm.srvc.user.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userNoteSet", propOrder = {
        "userNoteItem"
})
public class UserNoteSet {

    protected List<UserNoteSet.UserNoteItem> userNoteItem;

    public List<UserNoteSet.UserNoteItem> getUserNoteItem() {
        if (userNoteItem == null) {
            userNoteItem = new ArrayList<UserNoteSet.UserNoteItem>();
        }
        return this.userNoteItem;
    }



    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "userNote"
    })
    public static class UserNoteItem {

        protected UserNote userNote;

        /**
         * Gets the value of the userNote property.
         *
         * @return possible object is
         *         {@link UserNote }
         */
        public UserNote getUserNote() {
            return userNote;
        }

        /**
         * Sets the value of the userNote property.
         *
         *
         * @param value allowed object is
         *              {@link UserNote }
         */
        public void setUserNote(UserNote value) {
            this.userNote = value;
        }

    }

}
