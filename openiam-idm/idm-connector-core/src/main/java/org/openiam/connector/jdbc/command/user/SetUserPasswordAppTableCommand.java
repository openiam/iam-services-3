package org.openiam.connector.jdbc.command.user;

import org.apache.commons.lang.StringUtils;
import org.openiam.connector.jdbc.command.base.AbstractAppTableCommand;
import org.openiam.connector.jdbc.command.data.AppTableConfiguration;
import org.openiam.connector.type.ConnectorDataException;
import org.openiam.connector.type.constant.ErrorCode;
import org.openiam.connector.type.constant.StatusCodeType;
import org.openiam.connector.type.request.PasswordRequest;
import org.openiam.connector.type.response.ResponseType;
import org.openiam.script.ScriptIntegration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Map;

@Service("setPasswordAppTableCommand")
public class SetUserPasswordAppTableCommand extends AbstractAppTableCommand<PasswordRequest, ResponseType> {
    @Autowired
    @Qualifier("configurableGroovyScriptEngine")
    protected ScriptIntegration scriptRunner;

    @Override
    public ResponseType execute(PasswordRequest passwordRequest) throws ConnectorDataException {
        final ResponseType response = new ResponseType();
        response.setStatus(StatusCodeType.SUCCESS);

        AppTableConfiguration configuration = this.getConfiguration(passwordRequest.getTargetID());
        if (StringUtils.isBlank(configuration.getPrincipalPassword())) {
            String message = "Password synchronization is furned off! Need to add attributes: 'INCLUDE_IN_PASSWORD_SYNC' = 'Y' and 'PRINCIPAL_PASSWORD' = NAME OF PASSWORD COLUMN";
            log.warn(message);
            return response;
        }

        String modifiedPassword = null;
        if (StringUtils.isNotBlank(passwordRequest.getScriptHandler())) {
            try {
                Map<String, Object> bindingMap = new HashMap<String, Object>();
                bindingMap.put("password", passwordRequest.getPassword());
                modifiedPassword = (String) scriptRunner.execute(bindingMap, passwordRequest.getScriptHandler());
            } catch (Exception e) {
                log.error("Can't execute script", e);
            }
        }
        Connection con = this.getConnection(configuration.getManagedSys());

        PreparedStatement statement = null;
        try {
            statement = createChangeUserControlParamsStatement(con, configuration,
                    this.getTableName(configuration, this.getObjectType()), passwordRequest.getObjectIdentity(),
                    modifiedPassword == null ? passwordRequest.getPassword() : modifiedPassword, true);
            statement.executeUpdate();
            return response;
        } catch (Throwable e) {
            log.error(e.getMessage(), e);
            throw new ConnectorDataException(ErrorCode.CONNECTOR_ERROR, e.getMessage());
        } finally {
            this.closeStatement(statement);
            this.closeConnection(con);
        }
    }

    @Override
    protected String getObjectType() {
        return "USER";
    }

}
