package org.openiam.connector.ldap.dirtype;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openiam.base.AttributeOperationEnum;
import org.openiam.base.BaseAttribute;
import org.openiam.base.SysConfiguration;
import org.openiam.connector.type.request.CrudRequest;
import org.openiam.connector.type.request.PasswordRequest;
import org.openiam.connector.type.request.SuspendResumeRequest;
import org.openiam.idm.srvc.auth.domain.LoginEntity;
import org.openiam.idm.srvc.auth.login.LoginDataService;
import org.openiam.idm.srvc.mngsys.domain.ManagedSysEntity;
import org.openiam.idm.srvc.mngsys.dto.ManagedSystemObjectMatch;
import org.openiam.idm.srvc.pswd.service.PasswordGenerator;
import org.openiam.provision.type.ExtensibleAttribute;
import org.openiam.provision.type.ExtensibleObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import javax.naming.ldap.LdapContext;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EdirectoryImpl
        implements Directory {
    @Autowired
    private PasswordGenerator passwordGenerator;
    static final String PASSWORD_ATTRIBUTE = "userPassword";
    Map<String, Object> objectMap = new HashMap();
    private static final Log log = LogFactory.getLog(EdirectoryImpl.class);

    public ModificationItem[] resetPassword(PasswordRequest reqType)
            throws UnsupportedEncodingException {
        ModificationItem[] mods = new ModificationItem[1];
        String userPassword = getUserPassword(reqType.getExtensibleObject());
        if (StringUtils.isEmpty(userPassword)) {
            userPassword = reqType.getPassword();
        }
        mods[0] = new ModificationItem(2, new BasicAttribute("userPassword", userPassword));

        return mods;
    }

    public ModificationItem[] setPassword(PasswordRequest reqType)
            throws UnsupportedEncodingException {
        ModificationItem[] mods = new ModificationItem[1];
        String userPassword = getUserPassword(reqType.getExtensibleObject());
        if (StringUtils.isEmpty(userPassword)) {
            userPassword = reqType.getPassword();
        }
        mods[0] = new ModificationItem(2, new BasicAttribute("userPassword", userPassword));

        return mods;
    }

    public ModificationItem[] suspend(SuspendResumeRequest request) {
        String scrambledPswd = getScrambledPswd(request.getExtensibleObject());

        ModificationItem[] mods = new ModificationItem[1];
        mods[0] = new ModificationItem(2, new BasicAttribute("userPassword", scrambledPswd));

        return mods;
    }

    public ModificationItem[] resume(SuspendResumeRequest request) {
        String ldapName = (String) this.objectMap.get("LDAP_NAME");
        LoginDataService loginManager = (LoginDataService) this.objectMap.get("LOGIN_MANAGER");
        SysConfiguration sysConfiguration = (SysConfiguration) this.objectMap.get("CONFIGURATION");
        String targetID = (String) this.objectMap.get("TARGET_ID");
        try {
            String decPassword = getUserPassword(request.getExtensibleObject());
            if (StringUtils.isEmpty(decPassword)) {
                LoginEntity login = loginManager.getLoginByManagedSys(ldapName, targetID);
                String encPassword = login.getPassword();
                decPassword = loginManager.decryptPassword(login.getUserId(), encPassword);
            }
            ModificationItem[] mods = new ModificationItem[1];
            mods[0] = new ModificationItem(2, new BasicAttribute("userPassword", decPassword));
            return mods;
        } catch (Exception e) {
            log.error(e.toString());
        }
        return null;
    }

    public void delete(CrudRequest reqType, LdapContext ldapctx, String ldapName, String onDelete)
            throws NamingException {
        if ("DELETE".equalsIgnoreCase(onDelete)) {
            ldapctx.destroySubcontext(ldapName);
        } else if ("DISABLE".equalsIgnoreCase(onDelete)) {
            String scrambledPswd = getScrambledPswd(reqType.getExtensibleObject());

            ModificationItem[] mods = new ModificationItem[1];
            mods[0] = new ModificationItem(2, new BasicAttribute("userPassword", scrambledPswd));
            ldapctx.modifyAttributes(ldapName, mods);
        }
    }

    public void removeAccountMemberships(ManagedSysEntity managedSys, String identity, String identityDN, ManagedSystemObjectMatch matchObj, LdapContext ldapctx) {
        List<String> currentMembershipList = userMembershipList(managedSys, identityDN, matchObj, ldapctx);
        if (currentMembershipList != null) {
            for (String groupDN : currentMembershipList) {
                log.debug(String.format("Remove user with DN =[%s] from group with DN [%s]", identityDN, groupDN));

                ModificationItem[] modsUser = new ModificationItem[2];
                modsUser[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("groupMembership", groupDN));
                modsUser[1] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("securityEquals", groupDN));

                ModificationItem[] modsGroup = new ModificationItem[2];
                modsGroup[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("uniqueMember", identityDN));
                modsGroup[1] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("equivalentToMe", identityDN));
                try {
                    ldapctx.modifyAttributes(identityDN, modsUser);
                    ldapctx.modifyAttributes(groupDN, modsGroup);
                } catch (NamingException ne) {
                    log.error(ne);
                }
            }
        }
    }

    public void updateAccountMembership(ManagedSysEntity managedSys, List<BaseAttribute> newMembershipList, String identity, String identityDN, ManagedSystemObjectMatch matchObj, LdapContext ldapctx, ExtensibleObject obj) {
        List<String> currentMembershipList = userMembershipList(managedSys, identityDN, matchObj, ldapctx);
        if ((newMembershipList == null) && (currentMembershipList != null)) {
            for (String groupDN : currentMembershipList) {
                log.debug(String.format("Remove user with DN =[%s] from group with DN [%s]", identityDN, groupDN));
                ModificationItem[] modsUser = new ModificationItem[2];
                modsUser[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("groupMembership", groupDN));
                modsUser[1] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("securityEquals", groupDN));

                ModificationItem[] modsGroup = new ModificationItem[2];
                modsGroup[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("uniqueMember", identityDN));
                modsGroup[1] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("equivalentToMe", identityDN));
                try {
                    ldapctx.modifyAttributes(identityDN, modsUser);
                } catch (NamingException ne) {
                    log.error(ne);
                }
                try {
                    ldapctx.modifyAttributes(groupDN, modsGroup);
                } catch (NamingException ne) {
                    log.error(ne);
                }

            }
        }
        if (newMembershipList != null) {
            for (BaseAttribute ba : newMembershipList) {
                String groupDN = ba.getValue();
                boolean exists = isMemberOf(currentMembershipList, groupDN);
                if (ba.getOperationEnum() == AttributeOperationEnum.DELETE) {
                    if (exists) {
                        log.debug(String.format("Remove user with DN =[%s] from group with DN [%s]", identityDN, groupDN));

                        ModificationItem[] modsUser = new ModificationItem[2];
                        modsUser[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("groupMembership", groupDN));
                        modsUser[1] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("securityEquals", groupDN));

                        ModificationItem[] modsGroup = new ModificationItem[2];
                        modsGroup[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("uniqueMember", identityDN));
                        modsGroup[1] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("equivalentToMe", identityDN));

                        try {
                            ldapctx.modifyAttributes(identityDN, modsUser);
                            ldapctx.modifyAttributes(groupDN, modsGroup);
                        } catch (NamingException ne) {
                            log.error(ne);
                        }
                    }
                } else if ((ba.getOperationEnum() == null) || (ba.getOperationEnum() == AttributeOperationEnum.ADD) || (ba.getOperationEnum() == AttributeOperationEnum.NO_CHANGE)) {
                    if (!exists) {
                        log.debug(String.format("Add user with DN =[%s] to group with DN [%s]", identityDN, groupDN));
                        ModificationItem[] modsUser = new ModificationItem[2];
                        modsUser[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute("groupMembership", groupDN));
                        modsUser[1] = new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute("securityEquals", groupDN));

                        ModificationItem[] modsGroup = new ModificationItem[2];
                        modsGroup[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute("uniqueMember", identityDN));
                        modsGroup[1] = new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute("equivalentToMe", identityDN));

                        try {
                            ldapctx.modifyAttributes(identityDN, modsUser);
                        } catch (NamingException ne) {
                            log.error(ne);
                        }
                        try {
                            ldapctx.modifyAttributes(groupDN, modsGroup);
                        } catch (NamingException ne) {
                            doCleanup(ldapctx, identityDN, groupDN);
                            log.error(ne);
                        }
                    }
                }
            }
        }
        /* this sleep is needed for cases when group membership is changed at the same time when user is moved between ou's*/
        if (managedSys.getModifyHandler() != null)
            try {
                Thread.sleep(Long.parseLong(managedSys.getModifyHandler()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }

    public void removeSupervisorMemberships(ManagedSysEntity managedSys, String identity, String identityDN, ManagedSystemObjectMatch matchObj, LdapContext ldapctx) {
        List<String> currentSupervisorMembershipList = userSupervisorMembershipList(managedSys, identity, matchObj, ldapctx);
        if (currentSupervisorMembershipList != null) {
            for (String s : currentSupervisorMembershipList) {
                try {
                    if (log.isDebugEnabled()) {
                        log.debug("Removing supervisor: " + s + " from " + identityDN);
                    }
                    ModificationItem[] mods = new ModificationItem[1];
                    mods[0] = new ModificationItem(3, new BasicAttribute("manager", s));
                    ldapctx.modifyAttributes(identityDN, mods);
                } catch (NamingException ne) {
                    log.error(ne);
                }
            }
        }
    }

    private void doCleanup(LdapContext ldapctx, String identityDN, String value) {
        log.debug("Start doCleanup()...");
        ModificationItem[] modsUser = new ModificationItem[2];

        modsUser[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("groupMembership", value));
        modsUser[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("securityEquals", value));
        try {
            ldapctx.modifyAttributes(identityDN, modsUser);
        } catch (NamingException ne) {
            log.error(ne);
        }
    }

    public void updateSupervisorMembership(ManagedSysEntity managedSys, List<BaseAttribute> supervisorMembershipList, String identity, String identityDN, ManagedSystemObjectMatch matchObj, LdapContext ldapctx, ExtensibleObject obj) {
        List<String> currentSupervisorMembershipList = userSupervisorMembershipList(managedSys, identity, matchObj, ldapctx);
        if (log.isDebugEnabled()) {
            log.debug("Current ldap supervisor membership:" + currentSupervisorMembershipList);
        }
        if ((supervisorMembershipList == null) && (currentSupervisorMembershipList != null)) {
            for (String s : currentSupervisorMembershipList) {
                try {
                    if (log.isDebugEnabled()) {
                        log.debug("Removing supervisor: " + s + " from " + identityDN);
                    }
                    ModificationItem[] mods = new ModificationItem[1];
                    mods[0] = new ModificationItem(3, new BasicAttribute("manager", s));
                    ldapctx.modifyAttributes(identityDN, mods);
                } catch (NamingException ne) {
                    log.error(ne);
                }
            }
        }
        if (supervisorMembershipList != null) {
            for (BaseAttribute ba : supervisorMembershipList) {
                String supervisorName = ba.getName();
                boolean exists = isMemberOf(currentSupervisorMembershipList, supervisorName);
                if (ba.getOperationEnum() == AttributeOperationEnum.DELETE) {
                    if (exists) {
                        try {
                            ModificationItem[] mods = new ModificationItem[1];
                            mods[0] = new ModificationItem(3, new BasicAttribute("manager", supervisorName));
                            ldapctx.modifyAttributes(identityDN, mods);
                        } catch (NamingException ne) {
                            log.error(ne);
                        }
                    }
                } else if ((ba.getOperationEnum() == null) || (ba.getOperationEnum() == AttributeOperationEnum.ADD) || (ba.getOperationEnum() == AttributeOperationEnum.NO_CHANGE)) {
                    if (!exists) {
                        try {
                            ModificationItem[] mods = new ModificationItem[1];
                            mods[0] = new ModificationItem(1, new BasicAttribute("manager", supervisorName));
                            ldapctx.modifyAttributes(identityDN, mods);
                        } catch (NamingException ne) {
                            log.error(ne);
                        }
                    }
                }
            }
        }
    }

    public String readAttributeValue(ExtensibleObject extObject, String attributeName) {
        if ((extObject != null) && (CollectionUtils.isNotEmpty(extObject.getAttributes()))) {
            for (ExtensibleAttribute attr : extObject.getAttributes()) {
                if (attr.getName().equalsIgnoreCase(attributeName)) {
                    return attr.getValue();
                }
            }
        }
        return null;
    }

    public void setAttributes(String name, Object obj) {
        this.objectMap.put(name, obj);
    }

    protected boolean isMemberOf(List<String> membershipList, String objectName) {
        if ((membershipList == null) || (membershipList.isEmpty())) {
            return false;
        }
        for (String member : membershipList) {
            log.debug("member" + member);
            if (member.equalsIgnoreCase(objectName)) {
                return true;
            }
        }
        return false;
    }

    protected List<String> userMembershipList(ManagedSysEntity managedSys, String userDN, ManagedSystemObjectMatch matchObj, LdapContext ldapctx) {
        List<String> currentMembershipList = new ArrayList();

        String userSearchFilter = "(&(objectClass=groupOfUniqueNames)(member=" + userDN + "))";
        if (log.isDebugEnabled()) {
            log.debug("isMemberOf()...");
            log.debug(" - userDN=" + userDN);
            log.debug(" - SearchFilter =" + userSearchFilter);
        }
        String searchBase = matchObj.getSearchBaseDn();
        try {
            SearchControls ctls = new SearchControls();

            String[] userReturnedAtts = {"member"};
            ctls.setReturningAttributes(userReturnedAtts);
            ctls.setSearchScope(managedSys.getSearchScope().getValue());
            NamingEnumeration answer = ldapctx.search(searchBase, userSearchFilter, ctls);
            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();

                String objectName = sr.getNameInNamespace();
                if (log.isDebugEnabled()) {
                    log.debug("Adding to current membership list " + objectName);
                }
                currentMembershipList.add(objectName);
            }
        } catch (Exception e) {
            log.error("userMembershipList", e);
        }
        if (currentMembershipList.isEmpty()) {
            return null;
        }
        return currentMembershipList;
    }

    protected List<String> userSupervisorMembershipList(
            ManagedSysEntity managedSys, String userDN, ManagedSystemObjectMatch matchObj, LdapContext ldapctx) {

        List<String> currentSupervisorMembershipList = new ArrayList<String>();

        if (log.isDebugEnabled()) {
            log.debug("isManager()...");
            log.debug(" - userDN =" + userDN);
            log.debug(" - MembershipObjectDN=" + matchObj.getSearchBaseDn());
        }
        String searchBase = matchObj.getSearchBaseDn();
        String userSearchFilter = matchObj.getSearchFilterUnescapeXml();
        // replace the place holder in the search filter
        if (StringUtils.isNotBlank(userSearchFilter)) {
            userSearchFilter = userSearchFilter.replace("?", userDN);
        }

        try {

            SearchControls ctls = new SearchControls();

            String userReturnedAtts[] = {"manager"};
            ctls.setReturningAttributes(userReturnedAtts);
            ctls.setSearchScope(managedSys.getSearchScope().getValue());

            NamingEnumeration answer = ldapctx.search(searchBase, userSearchFilter, ctls);

            //Loop through the search results
            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();

                Attributes attrs = sr.getAttributes();
                if (attrs != null) {

                    try {
                        for (NamingEnumeration ae = attrs.getAll(); ae.hasMore(); ) {
                            Attribute attr = (Attribute) ae.next();

                            for (NamingEnumeration e = attr.getAll(); e.hasMore(); ) {
                                currentSupervisorMembershipList.add((String) e.next());
                            }
                        }
                    } catch (NamingException e) {
                        log.error("Problem listing membership: " + e.toString());
                    }
                }
            }

        } catch (Exception e) {
            log.error("userSupervisorMembershipList", e);
        }

        if (currentSupervisorMembershipList.isEmpty()) {
            return null;
        }

        return currentSupervisorMembershipList;

    }

    protected String getScrambledPswd(ExtensibleObject extObject) {
        String scrambledPswd = getUserPassword(extObject);
        if (StringUtils.isEmpty(scrambledPswd)) {
            scrambledPswd = PasswordGenerator.generatePassword(10);
        }
        return scrambledPswd;
    }

    private String getUserPassword(ExtensibleObject extObject) {
        String scrambledPswd = null;
        if ((extObject != null) && (CollectionUtils.isNotEmpty(extObject.getAttributes()))) {
            for (ExtensibleAttribute attr : extObject.getAttributes()) {
                if (attr.getName().equalsIgnoreCase("userPassword")) {
                    scrambledPswd = attr.getValue();
                    break;
                }
            }
        }
        return scrambledPswd;
    }
}