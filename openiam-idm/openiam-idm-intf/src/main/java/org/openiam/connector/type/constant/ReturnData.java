
package org.openiam.connector.type.constant;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;



@XmlType(name = "ReturnData")
@XmlEnum
public enum ReturnData {

    @XmlEnumValue("identifier")
    IDENTIFIER("identifier"),
    @XmlEnumValue("data")
    DATA("data"),
    @XmlEnumValue("everything")
    EVERYTHING("everything");
    private final String value;

    ReturnData(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReturnData fromValue(String v) {
        for (ReturnData c: ReturnData.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
