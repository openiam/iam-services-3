package org.openiam.connector.type.constant;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;



@XmlType(name = "StatusCodeType")
@XmlEnum
public enum StatusCodeType {

    @XmlEnumValue("success")
    SUCCESS("success"),

    @XmlEnumValue("failure")
    FAILURE("failure"),

    @XmlEnumValue("successMoreResultsToReturn")
    SUCCESS_MORE_RESULTS_TO_RETURN("successMoreResultsToReturn"),

    @XmlEnumValue("pending")
    PENDING("pending");
    private final String value;

    StatusCodeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StatusCodeType fromValue(String v) {
        for (StatusCodeType c: StatusCodeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}

