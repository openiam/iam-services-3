
package org.openiam.connector.type.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.openiam.provision.type.ExtensibleObject;
import org.openiam.provision.type.ExtensibleUser;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CrudRequest", propOrder = {

})
public class CrudRequest<ExtObject extends ExtensibleObject> extends RequestType<ExtObject>{


    public CrudRequest() {}




    public CrudRequest(String objectIdentity, String containerID,
                       String targetID, String hostUrl, String hostPort,
                       String hostLoginId, String hostLoginPassword, String operation,
                       ExtObject extensibleObject) {
        super();
        this.objectIdentity = objectIdentity;
        this.containerID = containerID;
        this.targetID = targetID;
        this.hostUrl = hostUrl;
        this.hostPort = hostPort;
        this.hostLoginId = hostLoginId;
        this.hostLoginPassword = hostLoginPassword;
        this.operation = operation;
        this.extensibleObject = extensibleObject;
    }

}
