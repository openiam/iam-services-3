package org.openiam.connector.type.request;

import org.openiam.idm.srvc.mngsys.dto.ManagedSysDto;
import org.openiam.idm.srvc.mngsys.dto.ProvisionConnectorDto;

import java.io.Serializable;

/**
 * Created by zaporozhec on 1/26/17.
 */
public class HornetQConnectorRequest implements Serializable {
    private ManagedSysDto managedSysDto;
    private ProvisionConnectorDto provisionConnectorDto;
    private RequestType requestType;
    private HornetQOperation operation;
    public ManagedSysDto getManagedSysDto() {
        return managedSysDto;
    }

    public void setManagedSysDto(ManagedSysDto managedSysDto) {
        this.managedSysDto = managedSysDto;
    }

    public ProvisionConnectorDto getProvisionConnectorDto() {
        return provisionConnectorDto;
    }

    public void setProvisionConnectorDto(ProvisionConnectorDto provisionConnectorDto) {
        this.provisionConnectorDto = provisionConnectorDto;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public HornetQOperation getOperation() {
        return operation;
    }

    public void setOperation(HornetQOperation operation) {
        this.operation = operation;
    }
}
