package org.openiam.connector.type.request;

import java.io.*;

/**
 * Created by zaporozhec on 1/26/17.
 */
public class HornetQEncryptionUtil implements HornetQEncryptionUtilIntf {

    public byte[] encrypt(HornetQConnectorRequest request) {
        byte[] retVal = null;
        if (request == null) {
            return retVal;
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(request);
            out.flush();
            retVal = bos.toByteArray();
        } catch (IOException ex) {
        } finally {
            try {
                bos.close();
            } catch (IOException ex) {
                // ignore close exception
            }
        }
        return retVal;
    }

    public HornetQConnectorRequest decrypt(byte[] request) {
        HornetQConnectorRequest retVal = null;
        ByteArrayInputStream bis = new ByteArrayInputStream(request);
        ObjectInput in = null;
        try {
            in = new ObjectInputStream(bis);
            retVal = (HornetQConnectorRequest) in.readObject();
        } catch (Exception ex) {
            // ignore close exception
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                // ignore close exception
            }
        }
        return retVal;
    }

    @Override
    public String getEncryptionAlgorithmName() {
        return this.getClass().getCanonicalName();
    }
}
