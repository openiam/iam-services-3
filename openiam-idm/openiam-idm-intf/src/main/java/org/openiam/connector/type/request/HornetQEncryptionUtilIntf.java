package org.openiam.connector.type.request;

/**
 * Created by zaporozhec on 1/26/17.
 */
public interface HornetQEncryptionUtilIntf {
    public byte[] encrypt(HornetQConnectorRequest request);

    public HornetQConnectorRequest decrypt(byte[] request);

    public String getEncryptionAlgorithmName();
}
