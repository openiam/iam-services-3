package org.openiam.connector.type.request;

import java.io.Serializable;

/**
 * Created by zaporozhec on 1/26/17.
 */
public enum HornetQOperation implements Serializable {
    ADD, UPDATE, LOOKUP, SEARCH, DELETE, SETPASSWORD, RESETPASSWORD, SUSPEND, RESUME, TEST,VALIDATEPASSWORD
}
