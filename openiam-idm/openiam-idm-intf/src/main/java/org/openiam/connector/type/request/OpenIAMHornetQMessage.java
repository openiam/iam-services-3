package org.openiam.connector.type.request;

import java.io.Serializable;

/**
 * Created by zaporozhec on 1/26/17.
 */
public class OpenIAMHornetQMessage implements Serializable {
    private byte[] object;
    private String encryptionAlgName;
    private long expirationTime;

    public OpenIAMHornetQMessage(HornetQConnectorRequest request, HornetQEncryptionUtilIntf encoding, long expirationTime) {
        object = encoding.encrypt(request);
        encryptionAlgName = encoding.getEncryptionAlgorithmName();
        this.expirationTime = System.currentTimeMillis() + expirationTime;
    }

    public String getEncryptionAlgName() {
        return encryptionAlgName;
    }

    public byte[] getObject() {
        return object;
    }

    public long getExpirationTime() {
        return expirationTime;
    }
}
