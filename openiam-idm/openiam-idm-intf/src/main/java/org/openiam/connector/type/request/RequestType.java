
package org.openiam.connector.type.request;

import org.openiam.idm.srvc.mngsys.domain.ManagedSysEntity;
import org.openiam.idm.srvc.mngsys.dto.ManagedSysDto;
import org.openiam.provision.type.ExtensibleObject;

import javax.xml.bind.annotation.*;
import java.io.Serializable;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseRequestType",
        propOrder = {
                "requestID",
                "executionMode",
                "targetID",
                "hostUrl",
                "hostPort",
                "hostLoginId",
                "hostLoginPassword",
                "baseDN",
                "containerID",
                "scriptHandler",
                "operation",
                "extensibleObject",
                "objectIdentity",
                "objectIdentityAttributeName",
                "searchScope",
                "handler5",
                "driverUrl",
                "connectionString",
                "addHandler",
                "modifyHandler",
                "deleteHandler",
                "passwordHandler",
                "suspendHandler",
                "resumeHandler",
                "searchHandler",
                "lookupHandler",
                "testConnectionHandler",
                "reconcileResourceHandler",
                "attributeNamesHandler",
                "searchFilter"
        })
@XmlSeeAlso({
        CrudRequest.class
})
public class RequestType<ExtObject extends ExtensibleObject> implements Serializable {

    @XmlElement(required = true)
    protected String requestID;
    protected String executionMode;
    protected String targetID;
    protected String hostUrl;
    protected String hostPort;
    protected String hostLoginId;
    protected String hostLoginPassword;
    protected String baseDN;
    protected String containerID;
    @XmlElement
    private String scriptHandler;
    /* Change to an enum */
    protected String operation;

    protected ExtObject extensibleObject;

    @XmlElement(required = true)
    protected String objectIdentity;

    protected String objectIdentityAttributeName;

    protected Integer searchScope;
    protected String handler5;

    protected String driverUrl;
    protected String connectionString;

    protected String addHandler;
    protected String modifyHandler;
    protected String deleteHandler;
    protected String passwordHandler;
    protected String suspendHandler;
    protected String resumeHandler;
    protected String searchHandler;
    protected String lookupHandler;
    protected String testConnectionHandler;
    protected String reconcileResourceHandler;
    protected String attributeNamesHandler;
    protected String searchFilter;

    public RequestType() {

    }

    public RequestType(String requestID, String executionMode) {
        this.requestID = requestID;
        this.executionMode = executionMode;
    }


    /**
     * Gets the value of the requestID property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Sets the value of the requestID property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

    /**
     * Gets the value of the executionMode property.
     *
     * @return possible object is String
     */
    public String getExecutionMode() {
        return executionMode;
    }

    /**
     * Sets the value of the executionMode property.
     *
     * @param value allowed object is String
     */
    public void setExecutionMode(String value) {
        this.executionMode = value;
    }

    public String getTargetID() {
        return targetID;
    }

    public void setTargetID(String targetID) {
        this.targetID = targetID;
    }

    public String getHostUrl() {
        return hostUrl;
    }

    public void setHostUrl(String hostUrl) {
        this.hostUrl = hostUrl;
    }

    public String getHostPort() {
        return hostPort;
    }

    public void setHostPort(String hostPort) {
        this.hostPort = hostPort;
    }

    public String getHostLoginId() {
        return hostLoginId;
    }

    public void setHostLoginId(String hostLoginId) {
        this.hostLoginId = hostLoginId;
    }

    public String getHostLoginPassword() {
        return hostLoginPassword;
    }

    public void setHostLoginPassword(String hostLoginPassword) {
        this.hostLoginPassword = hostLoginPassword;
    }

    public String getBaseDN() {
        return baseDN;
    }

    public void setBaseDN(String baseDN) {
        this.baseDN = baseDN;
    }

    public String getContainerID() {
        return containerID;
    }

    public void setContainerID(String containerID) {
        this.containerID = containerID;
    }

    public String getScriptHandler() {
        return scriptHandler;
    }

    public void setScriptHandler(String scriptHandler) {
        this.scriptHandler = scriptHandler;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public ExtObject getExtensibleObject() {
        return extensibleObject;
    }

    public void setExtensibleObject(ExtObject extensibleObject) {
        this.extensibleObject = extensibleObject;
    }

    public String getObjectIdentity() {
        return objectIdentity;
    }

    public void setObjectIdentity(String objectIdentity) {
        this.objectIdentity = objectIdentity;
    }

    public String getObjectIdentityAttributeName() {
        return objectIdentityAttributeName;
    }

    public void setObjectIdentityAttributeName(String objectIdentityAttributeName) {
        this.objectIdentityAttributeName = objectIdentityAttributeName;
    }

    public Integer getSearchScope() {
        return searchScope;
    }

    public void setSearchScope(Integer searchScope) {
        this.searchScope = searchScope;
    }

    public String getHandler5() {
        return handler5;
    }

    public void setHandler5(String handler5) {
        this.handler5 = handler5;
    }

    public String getDriverUrl() {
        return driverUrl;
    }

    public void setDriverUrl(String driverUrl) {
        this.driverUrl = driverUrl;
    }

    public String getConnectionString() {
        return connectionString;
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    public String getAddHandler() {
        return addHandler;
    }

    public void setAddHandler(String addHandler) {
        this.addHandler = addHandler;
    }

    public String getModifyHandler() {
        return modifyHandler;
    }

    public void setModifyHandler(String modifyHandler) {
        this.modifyHandler = modifyHandler;
    }

    public String getDeleteHandler() {
        return deleteHandler;
    }

    public void setDeleteHandler(String deleteHandler) {
        this.deleteHandler = deleteHandler;
    }

    public String getPasswordHandler() {
        return passwordHandler;
    }

    public void setPasswordHandler(String passwordHandler) {
        this.passwordHandler = passwordHandler;
    }

    public String getSuspendHandler() {
        return suspendHandler;
    }

    public void setSuspendHandler(String suspendHandler) {
        this.suspendHandler = suspendHandler;
    }

    public String getResumeHandler() {
        return resumeHandler;
    }

    public void setResumeHandler(String resumeHandler) {
        this.resumeHandler = resumeHandler;
    }

    public String getSearchHandler() {
        return searchHandler;
    }

    public void setSearchHandler(String searchHandler) {
        this.searchHandler = searchHandler;
    }

    public String getLookupHandler() {
        return lookupHandler;
    }

    public void setLookupHandler(String lookupHandler) {
        this.lookupHandler = lookupHandler;
    }

    public String getTestConnectionHandler() {
        return testConnectionHandler;
    }

    public void setTestConnectionHandler(String testConnectionHandler) {
        this.testConnectionHandler = testConnectionHandler;
    }

    public String getReconcileResourceHandler() {
        return reconcileResourceHandler;
    }

    public void setReconcileResourceHandler(String reconcileResourceHandler) {
        this.reconcileResourceHandler = reconcileResourceHandler;
    }

    public String getAttributeNamesHandler() {
        return attributeNamesHandler;
    }

    public void setAttributeNamesHandler(String attributeNamesHandler) {
        this.attributeNamesHandler = attributeNamesHandler;
    }

    public void setHandlers(ManagedSysDto mngSys) {
        addHandler = mngSys.getAddHandler();
        modifyHandler = mngSys.getModifyHandler();
        deleteHandler = mngSys.getDeleteHandler();
        passwordHandler = mngSys.getPasswordHandler();
        suspendHandler = mngSys.getSuspendHandler();
        resumeHandler = mngSys.getResumeHandler();
        searchHandler = mngSys.getSearchHandler();
        lookupHandler = mngSys.getLookupHandler();
        testConnectionHandler = mngSys.getTestConnectionHandler();
        reconcileResourceHandler = mngSys.getReconcileResourceHandler();
        attributeNamesHandler = mngSys.getAttributeNamesHandler();
    }

    public void setHandlers(ManagedSysEntity mngSys) {
        addHandler = mngSys.getAddHandler();
        modifyHandler = mngSys.getModifyHandler();
        deleteHandler = mngSys.getDeleteHandler();
        passwordHandler = mngSys.getPasswordHandler();
        suspendHandler = mngSys.getSuspendHandler();
        resumeHandler = mngSys.getResumeHandler();
        searchHandler = mngSys.getSearchHandler();
        lookupHandler = mngSys.getLookupHandler();
        testConnectionHandler = mngSys.getTestConnectionHandler();
        reconcileResourceHandler = mngSys.getReconcileResourceHandler();
        attributeNamesHandler = mngSys.getAttributeNamesHandler();
    }

    public String getSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(String searchFilter) {
        this.searchFilter = searchFilter;
    }
}
