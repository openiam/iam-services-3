
package org.openiam.connector.type.request;

import org.openiam.provision.type.ExtensibleUser;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SuspendResumeRequest", propOrder = {
        "effectiveDate"
})
public class SuspendResumeRequest extends RequestType<ExtensibleUser> {


    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;


    /**
     * Gets the value of the effectiveDate property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }
}
