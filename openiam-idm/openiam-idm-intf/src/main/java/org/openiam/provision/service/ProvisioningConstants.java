package org.openiam.provision.service;

/**
 * Constants used during provisioning
 */
public class ProvisioningConstants {
    public static final int SUCCESS = 1;
    public static final int FAIL =  0;
    public static final int VALIDATION_FAIL = 2;

    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_1 = 8001;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_2 = 8002;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_3 = 8003;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_4 = 8004;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_5 = 8005;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_6 = 8006;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_7 = 8007;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_8 = 8008;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_9 = 8009;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_10 = 8010;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_11 = 8011;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_12 = 8012;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_13 = 8013;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_14 = 8014;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_15 = 8015;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_16 = 8016;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_17 = 8017;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_18 = 8018;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_19 = 8019;
    public static final int FAIL_PREPROCESSOR_CUSTOM_ERROR_20 = 8020;

    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_1 = 9001;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_2 = 9002;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_3 = 9003;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_4 = 9004;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_5 = 9005;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_6 = 9006;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_7 = 9007;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_8 = 9008;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_9 = 9009;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_10 = 9010;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_11 = 9011;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_12 = 9012;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_13 = 9013;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_14 = 9014;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_15 = 9015;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_16 = 9016;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_17 = 9017;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_18 = 9018;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_19 = 9019;
    public static final int FAIL_POSTPROCESSOR_CUSTOM_ERROR_20 = 9020;

}
