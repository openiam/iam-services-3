package org.openiam.idm.srvc.recon.command.grp;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openiam.base.ws.Response;
import org.openiam.connector.type.request.CrudRequest;
import org.openiam.dozer.converter.ManagedSystemObjectMatchDozerConverter;
import org.openiam.idm.srvc.grp.dto.Group;
import org.openiam.idm.srvc.mngsys.domain.ManagedSystemObjectMatchEntity;
import org.openiam.idm.srvc.mngsys.dto.ManagedSysDto;
import org.openiam.idm.srvc.mngsys.dto.ManagedSystemObjectMatch;
import org.openiam.idm.srvc.mngsys.service.ManagedSystemService;
import org.openiam.idm.srvc.mngsys.ws.ManagedSystemWebService;
import org.openiam.idm.srvc.recon.dto.ReconciliationSituation;
import org.openiam.idm.srvc.user.dto.UserStatusEnum;
import org.openiam.provision.dto.ProvisionGroup;
import org.openiam.provision.service.ConnectorAdapter;
import org.openiam.provision.service.ObjectProvisionService;
import org.openiam.provision.type.ExtensibleAttribute;
import org.openiam.provision.type.ExtensibleUser;
import org.openiam.util.MuleContextProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("deleteResourceGroupCommand")
public class DeleteResourceGroupCommand extends BaseReconciliationGroupCommand {
    private static final Log log = LogFactory.getLog(DeleteResourceGroupCommand.class);

    @Autowired
    @Qualifier("groupProvision")
    private ObjectProvisionService<ProvisionGroup> provisionService;

    @Autowired
    private ManagedSystemWebService managedSysService;

    @Autowired
    protected ManagedSystemService managedSystemService;

    @Autowired
    private ManagedSystemObjectMatchDozerConverter managedSystemObjectMatchDozerConverter;

    @Autowired
    private ConnectorAdapter connectorAdapter;

    public DeleteResourceGroupCommand() {
    }

    @Override
    public boolean execute(ReconciliationSituation config, String principal, String mSysID, Group group, List<ExtensibleAttribute> attributes) {
    	if(log.isDebugEnabled()) {
	        log.debug("Entering DeleteResourceGroupCommand");
	        log.debug("Do delete for Group: " + principal);
    	}
		if(group == null) {
            ManagedSysDto mSys = managedSysService.getManagedSys(mSysID);
            if(log.isDebugEnabled()) {
            	log.debug("Calling delete with Remote connector");
            }
            CrudRequest<ExtensibleUser> request = new CrudRequest<ExtensibleUser>();
            request.setObjectIdentity(principal);
            request.setTargetID(mSysID);
            request.setHostLoginId(mSys.getUserId());
            request.setHostLoginPassword(mSys.getDecryptPassword());
            request.setHostUrl(mSys.getHostUrl());
            request.setScriptHandler(mSys.getDeleteHandler());
            request.setSearchScope(mSys.getSearchScope().getValue());
            request.setHandler5(mSys.getHandler5());
            request.setDriverUrl(mSys.getDriverUrl());
            request.setConnectionString(mSys.getConnectionString());
            request.setHandlers(mSys);

            ManagedSystemObjectMatch matchObj = null;
            List<ManagedSystemObjectMatchEntity> objList = managedSystemService.managedSysObjectParam(mSysID,
                    ManagedSystemObjectMatch.GROUP);
            if (CollectionUtils.isNotEmpty(objList)) {
                matchObj = managedSystemObjectMatchDozerConverter.convertToDTO(objList.get(0), false);
            }

            if (matchObj != null) {
                request.setBaseDN(matchObj.getBaseDn());
            }
            if (matchObj != null && StringUtils.isNotEmpty(matchObj.getKeyField())) {
                request.setObjectIdentityAttributeName(matchObj.getKeyField());
            }

            if(log.isDebugEnabled()) {
            	log.debug("Calling delete local connector");
            }
            connectorAdapter.deleteRequest(mSys, request);

            return true;
        }

		try {
			ProvisionGroup pGroup = new ProvisionGroup(group);
			pGroup.setSrcSystemId(mSysID);
			executeScript(config.getScript(), attributes, pGroup);
			//Reset source system flag from User to avoid ignoring Provisioning for this resource
			pGroup.setSrcSystemId(null);
			Response response = provisionService.delete(mSysID, pGroup.getId(), UserStatusEnum.DELETED, DEFAULT_REQUESTER_ID);
			return response.isSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
    }

}
