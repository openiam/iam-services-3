package org.openiam.provision.service;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openiam.connector.type.request.HornetQConnectorRequest;
import org.openiam.connector.type.request.HornetQEncryptionUtil;
import org.openiam.connector.type.request.OpenIAMHornetQMessage;
import org.openiam.connector.type.response.ResponseType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.*;

/**
 * Created by zaporozhec on 1/20/17.
 */
@Component("connectorQueueService")
public class ConnectorQueueService {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    @Qualifier(value = "connectorQueue")
    private Queue queue;

    private long lifeTime = 30000L;

    @Autowired
    @Qualifier(value = "connectorResponseQueue")
    private Queue connectorResponseQueue;

    private static final Log log = LogFactory.getLog(ConnectorQueueService.class);


    /**
     * Send and forgot
     *
     * @param request
     * @param id
     */
    private void send(final HornetQConnectorRequest request, final String id, final Queue replayTo) {
        jmsTemplate.send(queue, new MessageCreator() {
            public Message createMessage(Session session) throws JMSException {
                Message message = session.createObjectMessage(new OpenIAMHornetQMessage(request, new HornetQEncryptionUtil(), lifeTime));
                message.setJMSCorrelationID(id);
                message.setJMSReplyTo(replayTo);
                return message;
            }
        });
    }

    public void send(final HornetQConnectorRequest request) {
        send(request, RandomStringUtils.randomAlphanumeric(32), null);
    }

    /**
     * For Lookup and search. Sync messaging where we need a response
     *
     * @param request
     * @return
     */
    public <T extends ResponseType> T sendAndReceive(final HornetQConnectorRequest request, final Class<T> responseClass) {
        T responseType = null;
        final String id = RandomStringUtils.randomAlphanumeric(32);
        send(request, id, connectorResponseQueue);
        try {
            jmsTemplate.setReceiveTimeout(lifeTime);
            Message m = jmsTemplate.receiveSelected(connectorResponseQueue, "JMSCorrelationID='" + id + "'");
            if (m == null) {
                log.error("Timeout exception. Message didn't received!!");
                throw new JMSException("Timeout exception. Message didn't received!!");
            }
            log.debug(m.getJMSCorrelationID());
            if (!m.getJMSCorrelationID().equalsIgnoreCase(id)) {
                throw new JMSException("Not correct Response received. Another JMSCorrelationID! Could not be, but if happens, check name filtering.");
            }
            responseType = responseClass.cast(((ObjectMessage) m).getObject());
        } catch (JMSException e) {
            log.error(e);
        }
        return responseType;
    }
}
