
package org.openiam.idm.srvc.key.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "migrateDataResponse", propOrder = {
    "_return"
})
public class MigrateDataResponse {

    @XmlElement(name = "return")
    protected Response _return;

    /**
     * Gets the value of the return property.
     *
     * @return
     *     possible object is
     *     {@link Response }
     *
     */
    public Response getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     *
     * @param value
     *     allowed object is
     *     {@link Response }
     *
     */
    public void setReturn(Response value) {
        this._return = value;
    }

}
