
package org.openiam.idm.srvc.key.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Response", propOrder = {
    "errorCode",
    "responseValue",
    "errorText",
    "errorTokenList"
})
public class Response {

    protected String errorCode;
    protected Object responseValue;
    protected String errorText;
    @XmlElement(nillable = true)
    protected List<EsbErrorToken> errorTokenList;
    @XmlAttribute(name = "status", required = true)
    protected ResponseStatus status;

    /**
     * Gets the value of the errorCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the value of the errorCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setErrorCode(String value) {
        this.errorCode = value;
    }

    /**
     * Gets the value of the responseValue property.
     *
     * @return
     *     possible object is
     *     {@link Object }
     *
     */
    public Object getResponseValue() {
        return responseValue;
    }

    /**
     * Sets the value of the responseValue property.
     *
     * @param value
     *     allowed object is
     *     {@link Object }
     *
     */
    public void setResponseValue(Object value) {
        this.responseValue = value;
    }

    /**
     * Gets the value of the errorText property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getErrorText() {
        return errorText;
    }

    /**
     * Sets the value of the errorText property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setErrorText(String value) {
        this.errorText = value;
    }

    /**
     * Gets the value of the errorTokenList property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorTokenList property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorTokenList().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EsbErrorToken }
     *
     *
     */
    public List<EsbErrorToken> getErrorTokenList() {
        if (errorTokenList == null) {
            errorTokenList = new ArrayList<EsbErrorToken>();
        }
        return this.errorTokenList;
    }

    /**
     * Gets the value of the status property.
     *
     * @return
     *     possible object is
     *     {@link ResponseStatus }
     *
     */
    public ResponseStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     *
     * @param value
     *     allowed object is
     *     {@link ResponseStatus }
     *
     */
    public void setStatus(ResponseStatus value) {
        this.status = value;
    }

}
