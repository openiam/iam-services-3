
package org.openiam.idm.srvc.key.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "decryptData", propOrder = {
    "encryptedData"
})
public class DecryptData {

    protected String encryptedData;

    /**
     * Gets the value of the encryptedData property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEncryptedData() {
        return encryptedData;
    }

    /**
     * Sets the value of the encryptedData property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEncryptedData(String value) {
        this.encryptedData = value;
    }

}
