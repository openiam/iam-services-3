
package org.openiam.idm.srvc.key.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCookieKeyResponse", propOrder = {
    "_return"
})
public class GetCookieKeyResponse {

    @XmlElement(name = "return")
    protected byte[] _return;

    /**
     * Gets the value of the return property.
     *
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     *
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setReturn(byte[] value) {
        this._return = value;
    }

}
