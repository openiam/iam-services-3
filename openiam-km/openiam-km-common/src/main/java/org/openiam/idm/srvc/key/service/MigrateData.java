
package org.openiam.idm.srvc.key.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "migrateData", propOrder = {
    "secretKey"
})
public class MigrateData {

    protected String secretKey;

    /**
     * Gets the value of the secretKey property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSecretKey() {
        return secretKey;
    }

    /**
     * Sets the value of the secretKey property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSecretKey(String value) {
        this.secretKey = value;
    }

}
