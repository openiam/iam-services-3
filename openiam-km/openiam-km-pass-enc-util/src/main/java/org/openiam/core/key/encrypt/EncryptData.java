package org.openiam.core.key.encrypt;

import org.openiam.core.key.util.KmUtil;
import org.openiam.core.key.ws.KeyManagementWSClient;
import org.openiam.idm.srvc.key.service.Response;
import org.openiam.idm.srvc.key.service.ResponseStatus;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.Scanner;


public class EncryptData {

    public static void main(String[] args){
        String keyPass = null;
        String jksPass = null;
        String jksFile = null;
        String wsdlLocation = null;
        Scanner scanner = new Scanner(System.in);


        try {

            File f = new File("km-pass-enc-util.properties");
            if(f!=null && f.exists()){
                System.out.println("Properties are found. loading...");
                Properties jksProperties = new Properties();
                jksProperties.load(new FileInputStream(f));
                wsdlLocation = jksProperties.getProperty("km.ws.wsdl.location", KmUtil.DEFAULT_WSDL_LOCATION);
            } else{
                System.out.println("Properties not found. Prompt required data...");
                wsdlLocation = KmUtil.promtParameter(scanner, "Please enter a valid KeyManagementWS url (wsdl location):", KmUtil.DEFAULT_WSDL_LOCATION);
            }
            if(wsdlLocation==null || wsdlLocation.trim().isEmpty()){
                wsdlLocation=KmUtil.DEFAULT_WSDL_LOCATION;
            }
            System.out.println("Encrypting data ...");
            KeyManagementWSClient client = new  KeyManagementWSClient(wsdlLocation);
            if ((args != null) && (args.length > 0)) {
                String encData = client.encryptData(args[0]);
                System.out.println("ENC:" + encData);
            } else {
                System.out.println("Wrong input data ...");
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }finally {
            scanner.close();
        }
    }
}
