package org.openiam.idm.srvc.auth.service;

import org.openiam.idm.srvc.audit.dto.IdmAuditLog;
import org.openiam.idm.srvc.auth.context.AuthenticationContext;
import org.openiam.idm.srvc.auth.dto.LogoutRequest;
import org.openiam.idm.srvc.auth.dto.Subject;

public interface AuthenticationModule {

	void logout(final LogoutRequest request, final IdmAuditLog auditLog) throws Exception;
	Subject login(final AuthenticationContext context) throws Exception;
}
