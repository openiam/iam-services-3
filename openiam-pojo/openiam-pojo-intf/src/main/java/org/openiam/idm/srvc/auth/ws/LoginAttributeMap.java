package org.openiam.idm.srvc.auth.ws;

import org.openiam.idm.srvc.auth.dto.LoginAttribute;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoginAttributeMap", propOrder = {
        "loginAttributeEntry"
})
public class LoginAttributeMap {

    protected List<LoginAttributeMap.LoginAttributeEntry> loginAttributeEntry;

    /**
     * Gets the value of the organizationAttributeEntry property.
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the organizationAttributeEntry property.
     * For example, to add a new item, do as follows:
     * getOrganizationAttributeEntry().add(newItem);
     * Objects of the following type(s) are allowed in the list
     * {@link LoginAttributeMap.LoginAttributeEntry }
     */
    public List<LoginAttributeMap.LoginAttributeEntry> getLoginAttributeEntry() {
        if (loginAttributeEntry == null) {
            loginAttributeEntry = new ArrayList<LoginAttributeMap.LoginAttributeEntry>();
        }
        return this.loginAttributeEntry;
    }


    /**
     * Java class for anonymous complex type.
     * The following schema fragment specifies the expected content contained within this class.
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "loginAttribute"
    })
    public static class LoginAttributeEntry {

        protected LoginAttribute loginAttribute;
        @XmlAttribute
        protected String id;

        /**
         * Gets the value of the organizationAttribute property.
         *
         * @return possible object is
         * {@link LoginAttribute }
         */
        public LoginAttribute getLoginAttribute() {
            return loginAttribute;
        }

        /**
         * Sets the value of the organizationAttribute property.
         *
         * @param value allowed object is
         *              {@link LoginAttribute }
         */
        public void setLoginAttribute(LoginAttribute value) {
            this.loginAttribute = value;
        }

        /**
         * Gets the value of the id property.
         *
         * @return possible object is
         * {@link String }
         */
        public String getId() {
            return id;
        }

        /**
         * Sets the value of the id property.
         *
         * @param value allowed object is
         *              {@link String }
         */
        public void setId(String value) {
            this.id = value;
        }

    }

}
