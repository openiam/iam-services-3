package org.openiam.idm.srvc.cat.service;

import java.util.List;

import org.openiam.idm.srvc.cat.dto.Category;

/**
 * Service interface for categories.  Categories are used to provide a classification structure for some objects in OpenIAM.
 * To create a top level category, leave the parentId field as null. To create a child category, assign a parentId. The parentId should
 * be the ID of an existing category.
 * The category service supports internationalization. To store text in different languages, use the
 * code snippet below to associate additional languages to a category object.<br>
 *
 * @author suneet shah
 */
public interface CategoryDataService {

    /**
     * Adds a new category to the system. CategoryId should not be assigned since is auto-generated
     *
     * @param cat
     * @return
     */
    public Category addCategory(Category cat);

    /**
     * Updates an existing category
     *
     * @param cat
     */
    public void updateCategory(Category cat);

    /**
     * Removes a existing specified by the categoryId. If the nested flag is set to true, then
     * child categories will be deleted as well.
     *
     * @param categoryId
     * @param nested
     * @return Returns the number of records that have been deleted.
     */
    public int removeCategory(String categoryId, boolean nested);

    /**
     * Returns a category defined by CategoryId. Returns null if no category is found.
     *
     * @param categoryId
     * @return {@link Category}
     */
    public Category getCategory(String categoryId);

    /**
     * Returns all the categories starting with top level categories.
     *
     * @return list of {@link Category}
     */
    public List<Category> getAllCategories(boolean nested);

    /**
     * Get all categories for the specified categoryId.
     *
     * @param categoryId
     * @param nested
     * @return List of {@link  Category}
     */
    public List<Category> getChildCategories(String categoryId, boolean nested);
}
