package org.openiam.idm.srvc.pswd.dto;

import org.openiam.base.ws.Response;
import org.openiam.base.ws.ResponseStatus;
import org.openiam.idm.srvc.pswd.rule.PasswordRuleViolation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PasswordValidationResponse", propOrder = {
        "rules",
        "violations",
		"results"
})
public class PasswordValidationResponse extends Response {
	
	public PasswordValidationResponse() {}

	public PasswordValidationResponse(final ResponseStatus status) {
		super(status);
	}

	private List<PasswordRuleViolation> violations;
	private List<PasswordRule> rules;
	private Map<String, Boolean> results;

	public List<PasswordRule> getRules() {
		return rules;
	}

	public void setRules(List<PasswordRule> rules) {
		this.rules = rules;
	}

	public List<PasswordRuleViolation> getViolations() {
		return violations;
	}

	public void setViolations(List<PasswordRuleViolation> violations) {
		this.violations = violations;
	}
	
	public void addViolation(final PasswordRuleViolation violation) {
		if(violation != null) {
			if(this.violations == null) {
				this.violations = new LinkedList<>();
			}
			this.violations.add(violation);
		}
	}

	public Map<String, Boolean> getResults() {
		return results;
	}

	public void setResults(Map<String, Boolean> results) {
		this.results = results;
	}

	public void addResult(String managedSysId, Boolean result) {
		if (managedSysId != null && result != null) {
			if (this.results == null) {
				this.results = new LinkedHashMap<>();
			}
			this.results.put(managedSysId, result);
		}
	}
}
