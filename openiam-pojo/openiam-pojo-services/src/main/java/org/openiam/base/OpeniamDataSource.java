package org.openiam.base;

import com.mchange.v2.c3p0.AbstractComboPooledDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.openiam.idm.srvc.key.service.JksManager;
import org.openiam.util.UserUtils;
import org.openiam.util.encrypt.Cryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import javax.naming.Referenceable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class OpeniamDataSource extends AbstractComboPooledDataSource implements Serializable, Referenceable {
    private static final long serialVersionUID = 1L;
    private static final short VERSION = 2;
    private static final Log log = LogFactory.getLog(UserUtils.class);

    private JksManager jksManager;
    private Integer iterationCount;

    @Value("${iam.jks.path}")
    private String jksFile;
    @Value("${iam.jks.password}")
    private String jksPassword;
    @Value("${iam.jks.common.key.password}")
    private String commonKeyPassword;

    @Autowired
    private Cryptor cryptor;

//    @Autowired
//    protected KeyManagementService keyManagementService;

    public OpeniamDataSource() {
        if (!StringUtils.hasText(this.jksPassword)) {
            this.jksPassword = JksManager.KEYSTORE_DEFAULT_PASSWORD;
        }
    }

    public OpeniamDataSource(boolean autoregister) {
        super(autoregister);
    }

    public OpeniamDataSource(String configName) {
        super(configName);
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.writeShort(2);
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        short version = ois.readShort();
        switch(version) {
            case 2:
                return;
            default:
                throw new IOException("Unsupported Serialized Version: " + version);
        }
    }

    @Override
    public void setPassword(String password) {
        if ((password != null) && (password.startsWith("ENC:"))) {
            try {
                String pass = password.substring(4);
                super.setPassword(decryptData(pass));
            } catch (Exception e) {
                log.error("Can't decrypt password:" + e.getMessage());
            }
        } else {
            super.setPassword(password);
        }
    }

    public void checkJksManager() {
        if (jksManager == null) {
            if (this.iterationCount == null) {
                jksManager = new JksManager(this.jksFile);
            } else {
                jksManager = new JksManager(this.jksFile, this.iterationCount);
            }
        }
    }

    private byte[] getPrimaryKey(String alias, String keyPassword) throws Exception {
        this.checkJksManager();
        return jksManager.getPrimaryKeyFromJKS(alias, jksPassword.toCharArray(), keyPassword.toCharArray());
    }

    public byte[] getCommonKey() throws Exception {
        byte[] key = this.getPrimaryKey(JksManager.KEYSTORE_COMMON_ALIAS, this.commonKeyPassword);
        if (key == null || key.length == 0) {
            return generateCommonKey();
        }
        return key;
    }

    public byte[] generateCommonKey() throws Exception {
        return generateJKSKey(this.commonKeyPassword, JksManager.KEYSTORE_COMMON_ALIAS);
    }

    private byte[] generateJKSKey(String keyPassword, String keystoreAlias) throws Exception {
        checkJksManager();
        jksManager.generatePrimaryKey(this.jksPassword.toCharArray(), keyPassword.toCharArray(), keystoreAlias);
        return this.getPrimaryKey(keystoreAlias, keyPassword);
    }

    public String decryptData(String encryptedData)throws Exception{
        byte[] dataKey = null;
        dataKey = this.getCommonKey();

        if(dataKey==null)
            throw new Exception("Cannot decrypt data, due to invalid secret key");
        if(!StringUtils.hasText(encryptedData))
            return encryptedData;
        return this.decrypt(dataKey, encryptedData);
    }

    public String decrypt(byte[] key, String encryptedData)throws Exception{
        if(key!=null && key.length>0){
            return cryptor.decrypt(key, encryptedData);
        }
        if(log.isDebugEnabled()) {
            log.debug("Data Key is null. Skipping decryption...");
        }
        return null;
    }
}
