package org.openiam.idm.srvc.access.service;

import org.apache.commons.collections4.CollectionUtils;
import org.openiam.base.KeyDTO;
import org.openiam.base.domain.KeyEntity;
import org.openiam.idm.searchbeans.ResourceSearchBean;
import org.openiam.idm.searchbeans.RoleSearchBean;
import org.openiam.idm.srvc.access.domain.AccessRightEntity;
import org.openiam.idm.srvc.entitlements.AbstractEntitlementsDTO;
import org.openiam.idm.srvc.membership.domain.AbstractMembershipXrefEntity;
import org.openiam.idm.srvc.res.domain.ResourceEntity;
import org.openiam.idm.srvc.res.dto.Resource;
import org.openiam.idm.srvc.role.domain.RoleEntity;
import org.openiam.idm.srvc.role.dto.Role;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * This class processes entitlement rights before returning them to the caller
 * By definition, we have the following definiitions:
 * Org to Group
 * Group to Role
 * Role to Resource
 * Org to Role
 * Org to Resource
 * Group to Resource
 * Parent Group to Child Group
 * Parent Role to Child Role
 * Parent Resource to Child Resource
 * Access Rights are uni-directional, and are enforced FROM the parent TO the child.
 * The meaning of the access rights depends on how you request it.
 * It is up to the caller to interpret the meaning
 *
 * @author lbornova
 */
@Component
public class AccessRightProcessor {

    private <T extends KeyDTO> Map<String, T> transform(final List<T> dtoList) {
        Map<String, T> retVal = new HashMap<>();
        if (CollectionUtils.isNotEmpty(dtoList)) {
            for (T t : dtoList) {
                retVal.put(t.getId(), t);
            }
        }
        return retVal;

    }

    private <T extends AbstractEntitlementsDTO> void setRights(final Map<String, T> dtoMap, final AbstractMembershipXrefEntity<?, ?> xref, final KeyEntity entity) {
        if (xref != null && xref.getRights() != null) {
            final List<String> rightIds = new ArrayList<>();
            for (AccessRightEntity e : xref.getRights()) {
                rightIds.add(e.getId());
            }
            dtoMap.get(entity.getId()).setAccessRightIds(rightIds);
        }
    }

    private void assertLength(final Set<String> set) {
        if (CollectionUtils.size(set) > 1) {
            throw new IllegalArgumentException("When querying for rights, you can only query by one object at a time.  See stacktrace for details as to where this happens");
        }
    }


    public void process(final RoleSearchBean searchBean, final List<Role> dtoList, final List<RoleEntity> entityList) {
        if (searchBean != null) {
            if (searchBean.isIncludeAccessRights()) {
                if (CollectionUtils.isNotEmpty(entityList)) {
                    final Map<String, Role> dtoMap = transform(dtoList);
                    for (RoleEntity entity : entityList) {
                        if (CollectionUtils.isNotEmpty(searchBean.getParentIdSet())) {
                            assertLength(searchBean.getParentIdSet());
                            final String entityId = searchBean.getParentIdSet().iterator().next();
                            final AbstractMembershipXrefEntity xref = entity.getParent(entityId);
                            setRights(dtoMap, xref, entity);
                        } else if (CollectionUtils.isNotEmpty(searchBean.getChildIdSet())) {
                            assertLength(searchBean.getChildIdSet());
                            final String entityId = searchBean.getChildIdSet().iterator().next();
                            final AbstractMembershipXrefEntity xref = entity.getChild(entityId);
                            setRights(dtoMap, xref, entity);
                        } else if (CollectionUtils.isNotEmpty(searchBean.getResourceIdSet())) {
                            assertLength(searchBean.getResourceIdSet());
                            final String entityId = searchBean.getResourceIdSet().iterator().next();
                            final AbstractMembershipXrefEntity xref = entity.getResource(entityId);
                            setRights(dtoMap, xref, entity);
                        }

                    }
                }
            }
        }
    }

    public void process(final ResourceSearchBean searchBean, final List<Resource> dtoList, final List<ResourceEntity> entityList) {
        if (searchBean != null) {
            if (searchBean.isIncludeAccessRights()) {
                if (CollectionUtils.isNotEmpty(entityList)) {
                    final Map<String, Resource> dtoMap = transform(dtoList);
                    for (ResourceEntity entity : entityList) {
                        if (CollectionUtils.isNotEmpty(searchBean.getParentIdSet())) {
                            assertLength(searchBean.getParentIdSet());
                            final String entityId = searchBean.getParentIdSet().iterator().next();
                            final AbstractMembershipXrefEntity xref = entity.getParent(entityId);
                            setRights(dtoMap, xref, entity);
                        } else if (CollectionUtils.isNotEmpty(searchBean.getChildIdSet())) {
                            assertLength(searchBean.getChildIdSet());
                            final String entityId = searchBean.getChildIdSet().iterator().next();
                            final AbstractMembershipXrefEntity xref = entity.getChild(entityId);
                            setRights(dtoMap, xref, entity);
                        } else if (CollectionUtils.isNotEmpty(searchBean.getRoleIdSet())) {
                            assertLength(searchBean.getRoleIdSet());
                            final String entityId = searchBean.getRoleIdSet().iterator().next();
                            final AbstractMembershipXrefEntity xref = entity.getRole(entityId);
                            setRights(dtoMap, xref, entity);
                        }
                    }
                }
            }
        }
    }
}
