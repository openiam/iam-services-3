package org.openiam.idm.srvc.continfo.service;

// Generated Jun 12, 2007 10:46:15 PM by Hibernate Tools 3.2.0.beta8

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.openiam.base.ws.SearchParam;
import org.openiam.core.dao.BaseDaoImpl;
import org.openiam.idm.searchbeans.AbstractSearchBean;
import org.openiam.idm.searchbeans.EmailSearchBean;
import org.openiam.idm.searchbeans.SearchBean;
import org.openiam.idm.searchbeans.UserSearchBean;
import org.openiam.idm.srvc.continfo.domain.EmailAddressEntity;
import org.openiam.idm.srvc.res.domain.ResourceEntity;
import org.openiam.idm.srvc.res.domain.ResourceTypeEntity;
import org.springframework.stereotype.Repository;

import java.util.*;

import javax.annotation.PostConstruct;

@Repository("emailAddressDAO")
public class EmailAddressDAOImpl extends BaseDaoImpl<EmailAddressEntity, String> implements EmailAddressDAO {

	private static final Log log = LogFactory.getLog(AddressDAOImpl.class);
	
	private String DELETE_BY_USER_ID = "DELETE FROM %s e WHERE e.parent.id = :userId";
	
	@PostConstruct
	public void initSQL() {
		DELETE_BY_USER_ID = String.format(DELETE_BY_USER_ID, domainClass.getSimpleName());
	}

    protected Criteria getExampleCriteria(final SearchBean searchBean) {
        Criteria criteria = getCriteria();
        if (searchBean != null && searchBean instanceof EmailSearchBean) {
            final EmailSearchBean emailSearchBean = (EmailSearchBean) searchBean;

            if (StringUtils.isNotBlank(emailSearchBean.getKey())) {
                criteria.add(Restrictions.eq(getPKfieldName(), emailSearchBean.getKey()));
            } else {
                if (StringUtils.isNotEmpty(emailSearchBean.getName())) {
                    String emailName = emailSearchBean.getName();
                    MatchMode matchMode = null;
                    if (StringUtils.indexOf(emailName, "*") == 0) {
                        matchMode = MatchMode.END;
                        emailName = emailName.substring(1);
                    }
                    if (StringUtils.isNotEmpty(emailName) && StringUtils.indexOf(emailName, "*") == emailName.length() - 1) {
                        emailName = emailName.substring(0, emailName.length() - 1);
                        matchMode = (matchMode == MatchMode.END) ? MatchMode.ANYWHERE : MatchMode.START;
                    }

                    if (StringUtils.isNotEmpty(emailName)) {
                        if (matchMode != null) {
                            criteria.add(Restrictions.ilike("name", emailName, matchMode));
                        } else {
                            criteria.add(Restrictions.eq("name", emailName));
                        }
                    }
                }

                if (emailSearchBean.getParentId() != null) {
                    if (StringUtils.isNotBlank(emailSearchBean.getParentId())) {
                        criteria.add(Restrictions.eq("parent.id", emailSearchBean.getParentId()));
                    }
                }

                if (emailSearchBean.getMetadataTypeId() != null) {
                    if (StringUtils.isNotBlank(emailSearchBean.getMetadataTypeId())) {
                        criteria.add(Restrictions.eq("metadataType.id", emailSearchBean.getMetadataTypeId()));
                    }
                }

                final SearchParam param = emailSearchBean.getEmailMatchToken();
                if (param != null && param.isValid()) {
                    final String value = StringUtils.trimToNull(param.getValue());
                    if (value != null) {
                        switch (param.getMatchType()) {
                            case EXACT:
                                criteria.add(Restrictions.eq("emailAddress", StringUtils.lowerCase(value)));
                                break;
                            case STARTS_WITH:
                                criteria.add(Restrictions.ilike("emailAddress", value.toLowerCase(), MatchMode.START));
                                break;
                            default:
                                break;
                        }
                    }
                }

/*                if (StringUtils.isNotBlank(emailSearchBean.getEmailMatchToken().getValue())) {
                    criteria.add(Restrictions.eq("emailAddress", emailSearchBean.getEmailMatchToken().getValue()));
                }*/
            }
        }
        return criteria;
    }

    @Override
    public void removeByUserId(final String userId) {
    	final Query qry = getSession().createQuery(DELETE_BY_USER_ID);
		qry.setString("userId", userId);
		qry.executeUpdate();
	}


	@Override
	protected String getPKfieldName() {
		return "emailId";
	}

    public List<EmailAddressEntity> getByExample(final SearchBean searchBean) {
        return getByExample(searchBean, -1, -1);
    }

    public List<EmailAddressEntity> getByExample(final SearchBean searchBean, int from, int size) {
        final Criteria criteria = getExampleCriteria(searchBean);
        if (from > -1) {
            criteria.setFirstResult(from);
        }

        if (size > -1) {
            criteria.setMaxResults(size);
        }

        if (searchBean instanceof AbstractSearchBean) {
            AbstractSearchBean sb = (AbstractSearchBean) searchBean;
//            if (StringUtils.isNotBlank(sb.getSortBy())) {
//                criteria.addOrder(sb.getOrderBy().equals(OrderConstants.DESC) ?
//                        Order.desc(sb.getSortBy()) :
//                        Order.asc(sb.getSortBy()));
//            }

            if (CollectionUtils.isNotEmpty(sb.getSortBy())) {
                this.setOderByCriteria(criteria, sb);
            }
        }
        return (List<EmailAddressEntity>) criteria.setCacheable(this.cachable()).list();
    }
}
