package org.openiam.idm.srvc.continfo.service;


import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.openiam.base.ws.SearchParam;
import org.openiam.core.dao.BaseDaoImpl;
import org.openiam.idm.searchbeans.AbstractSearchBean;
import org.openiam.idm.searchbeans.EmailSearchBean;
import org.openiam.idm.searchbeans.PhoneSearchBean;
import org.openiam.idm.searchbeans.SearchBean;
import org.openiam.idm.srvc.continfo.domain.EmailAddressEntity;
import org.openiam.idm.srvc.continfo.domain.PhoneEntity;
import org.springframework.stereotype.Repository;

import java.util.*;

import javax.annotation.PostConstruct;


@Repository("phoneDAO")
public class PhoneDAOImpl extends BaseDaoImpl<PhoneEntity, String> implements PhoneDAO {

	private static final Log log = LogFactory.getLog(PhoneDAOImpl.class);
	
	private String DELETE_BY_USER_ID = "DELETE FROM %s e WHERE e.parent.id = :userId";
	
	@PostConstruct
	public void initSQL() {
		DELETE_BY_USER_ID = String.format(DELETE_BY_USER_ID, domainClass.getSimpleName());
	}

    @Override
    protected Criteria getExampleCriteria(PhoneEntity phone){
        final Criteria criteria = getCriteria();
        if (StringUtils.isNotBlank(phone.getPhoneId())) {
            criteria.add(Restrictions.eq(getPKfieldName(), phone.getPhoneId()));
        } else {

            if (phone.getParent() != null) {
                if (StringUtils.isNotBlank(phone.getParent().getId())) {
                    criteria.add(Restrictions.eq("parent.id", phone.getParent().getId()));
                }
            }

            if (phone.getMetadataType() != null) {
                if (StringUtils.isNotBlank(phone.getMetadataType().getId())) {
                    criteria.add(Restrictions.eq("metadataType.id", phone.getMetadataType().getId()));
                }
            }

        }
        return criteria;
    }

    @Override
    public void removeByUserId(final String userId) {
    	final Query qry = getSession().createQuery(DELETE_BY_USER_ID);
		qry.setString("userId", userId);
		qry.executeUpdate();
	}

	@Override
	protected String getPKfieldName() {
		return "phoneId";
	}

    public List<PhoneEntity> getByExample(final SearchBean searchBean) {
        log.info("PhoneDAOImpl getByExample(SearchBean)");
        return getByExample(searchBean, -1, -1);
    }

    public List<PhoneEntity> getByExample(final SearchBean searchBean, int from, int size) {
        log.info("PhoneDAOImpl getByExample(SearchBean, int, int)");
        final Criteria criteria = getExampleCriteria(searchBean);
        if (from > -1) {
            criteria.setFirstResult(from);
        }

        if (size > -1) {
            criteria.setMaxResults(size);
        }

        if (searchBean instanceof AbstractSearchBean) {
            AbstractSearchBean sb = (AbstractSearchBean) searchBean;
//            if (StringUtils.isNotBlank(sb.getSortBy())) {
//                criteria.addOrder(sb.getOrderBy().equals(OrderConstants.DESC) ?
//                        Order.desc(sb.getSortBy()) :
//                        Order.asc(sb.getSortBy()));
//            }

            if (CollectionUtils.isNotEmpty(sb.getSortBy())) {
                this.setOderByCriteria(criteria, sb);
            }
        }
        return (List<PhoneEntity>) criteria.setCacheable(this.cachable()).list();
    }

    protected Criteria getExampleCriteria(final SearchBean searchBean) {
        log.info("PhoneDAOImpl getExampleCriteria(SearchBean)");
        Criteria criteria = getCriteria();
        if (searchBean != null && searchBean instanceof PhoneSearchBean) {
            final PhoneSearchBean phoneSearchBean = (PhoneSearchBean) searchBean;

            if (StringUtils.isNotBlank(phoneSearchBean.getKey())) {
                criteria.add(Restrictions.eq(getPKfieldName(), phoneSearchBean.getKey()));
            } else {
                /*if (StringUtils.isNotEmpty(phoneSearchBean.getPhoneNbr())) {
                    String phoneName = phoneSearchBean.getPhoneNbr();
                    MatchMode matchMode = null;
                    if (StringUtils.indexOf(phoneName, "*") == 0) {
                        matchMode = MatchMode.END;
                        phoneName = phoneName.substring(1);
                    }
                    if (StringUtils.isNotEmpty(phoneName) && StringUtils.indexOf(phoneName, "*") == phoneName.length() - 1) {
                        phoneName = phoneName.substring(0, phoneName.length() - 1);
                        matchMode = (matchMode == MatchMode.END) ? MatchMode.ANYWHERE : MatchMode.START;
                    }

                    if (StringUtils.isNotEmpty(phoneName)) {
                        if (matchMode != null) {
                            criteria.add(Restrictions.ilike("name", phoneName, matchMode));
                        } else {
                            criteria.add(Restrictions.eq("name", phoneName));
                        }
                    }
                }*/

                if (phoneSearchBean.getPhoneNbr() != null) {
                    if (StringUtils.isNotBlank(phoneSearchBean.getPhoneNbr())){
                        criteria.add(Restrictions.eq("phoneNbr", phoneSearchBean.getPhoneNbr()));
                    }
                }

                if (phoneSearchBean.getPhoneAreaCd() != null) {
                    if (StringUtils.isNotBlank(phoneSearchBean.getPhoneAreaCd())){
                        criteria.add(Restrictions.eq("areaCd", phoneSearchBean.getPhoneAreaCd()));
                    }
                }

                if (phoneSearchBean.getParentId() != null) {
                    if (StringUtils.isNotBlank(phoneSearchBean.getParentId())) {
                        criteria.add(Restrictions.eq("parent.id", phoneSearchBean.getParentId()));
                    }
                }

                if (phoneSearchBean.getMetadataTypeId() != null) {
                    if (StringUtils.isNotBlank(phoneSearchBean.getMetadataTypeId())) {
                        criteria.add(Restrictions.eq("metadataType.id", phoneSearchBean.getMetadataTypeId()));
                    }
                }
            }
        }
        return criteria;
    }
}
