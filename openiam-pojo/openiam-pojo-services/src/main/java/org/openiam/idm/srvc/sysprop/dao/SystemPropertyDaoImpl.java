package org.openiam.idm.srvc.sysprop.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.openiam.core.dao.BaseDaoImpl;
import org.openiam.idm.srvc.sysprop.domain.SystemPropertyEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("systemPropertyDao")
public class SystemPropertyDaoImpl extends BaseDaoImpl<SystemPropertyEntity, String> implements SystemPropertyDao {

    @Override
    protected String getPKfieldName() {
        return "name";
    }

    protected boolean cachable() {
        return false;
    }

    @Override
    public List<SystemPropertyEntity> getByMetadataType(String mdTypeId) {
        if (mdTypeId == null) {
            throw new NullPointerException("Metadata type id is null");
        }
        final Criteria criteria = getCriteria();
        return (List<SystemPropertyEntity>) criteria.add(Restrictions.eq("type.id", mdTypeId)).list();
    }

    @Override
    public List<SystemPropertyEntity> getByName(String name) {
        if (name == null) {
            throw new NullPointerException("name is null");
        }
        final Criteria criteria = getCriteria();
        criteria.add(Restrictions.ilike("name", name, MatchMode.START));
        criteria.setFirstResult(0);
        criteria.setMaxResults(Integer.MAX_VALUE);
        criteria.setCacheable(this.cachable());
        return (List<SystemPropertyEntity>) criteria.list();
    }
}
