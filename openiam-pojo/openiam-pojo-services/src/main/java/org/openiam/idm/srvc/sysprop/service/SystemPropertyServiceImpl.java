package org.openiam.idm.srvc.sysprop.service;

import org.openiam.idm.srvc.meta.service.MetadataTypeDAO;
import org.openiam.idm.srvc.sysprop.dao.SystemPropertyDao;
import org.openiam.idm.srvc.sysprop.domain.SystemPropertyEntity;
import org.openiam.idm.srvc.sysprop.dto.SystemPropertyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author zaporozhec
 */
@Service("systemPropertyService")
public class SystemPropertyServiceImpl implements SystemPropertyService {
    @Autowired
    @Qualifier("systemPropertyDao")
    private SystemPropertyDao systemPropertyDao;
    @Autowired
    private MetadataTypeDAO metadataTypeDao;

    @Override
    @Transactional(readOnly = true)
    @Cacheable(value = "systemProperties", key = "{#name}")
    public List<SystemPropertyDto> getByName(String name) {
        List<SystemPropertyEntity> list = systemPropertyDao.getByName(name);
        if (list == null) {
            return null;
        } else {
            return SystemPropertyEntity.toDtoList(list);
        }
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable(value = "systemProperties", key = "{#mdTypeId}")
    public List<SystemPropertyDto> getByType(String mdTypeId) {
        List<SystemPropertyEntity> list = systemPropertyDao.getByMetadataType(mdTypeId);
        if (list == null) {
            return null;
        } else {
            return SystemPropertyEntity.toDtoList(list);
        }
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable(value = "systemProperties", key = "{#id}")
    public SystemPropertyDto getById(String id) {
        SystemPropertyEntity entity = systemPropertyDao.findById(id);
        if (entity == null) {
            return null;
        } else {
            return entity.toDTO();
        }
    }

    @Override
    @Transactional
    @CacheEvict(value = "systemProperties", allEntries = true)
    public void save(SystemPropertyDto propertyDto) {
        //Now only Value changes is allowed
        //FIXME
        if (propertyDto != null) {
            SystemPropertyEntity dbEntity = systemPropertyDao.findById(propertyDto.getName());
            if (dbEntity == null) {
                throw new NullPointerException("No such System property");
            }
            dbEntity.setValue(propertyDto.getValue());
            systemPropertyDao.save(dbEntity);
        }
    }

    @Override
    @Transactional
    @Cacheable(value = "systemProperties")
    public List<SystemPropertyDto> getAll() {
        return SystemPropertyEntity.toDtoList(systemPropertyDao.findAll());
    }
}
