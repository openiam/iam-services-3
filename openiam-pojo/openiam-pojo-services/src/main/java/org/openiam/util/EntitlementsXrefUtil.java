package org.openiam.util;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.openiam.base.domain.AbstractMetdataTypeEntity;
import org.openiam.idm.srvc.access.domain.AccessRightEntity;
import org.openiam.idm.srvc.membership.domain.AbstractMembershipXrefEntity;
import org.openiam.idm.srvc.meta.domain.MetadataTypeEntity;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by zaporozhec on 2/16/17.
 */
public class EntitlementsXrefUtil {


    public static void mergeAccessRights(MetadataTypeEntity metadataType, Set<? extends AbstractMembershipXrefEntity> rightEntities) {

        if (CollectionUtils.isEmpty(rightEntities))
            return;

        String metadataTypeId = metadataType == null ? null : metadataType.getId();
        Iterator<? extends AbstractMembershipXrefEntity> rightEntitiesIterator = rightEntities.iterator();
        while (rightEntitiesIterator.hasNext()) {
            AbstractMembershipXrefEntity xrefEntity = rightEntitiesIterator.next();
            if (CollectionUtils.isNotEmpty(xrefEntity.getRights())) {
                Iterator<AccessRightEntity> accessRightEntityIterator = xrefEntity.getRights().iterator();
                while (accessRightEntityIterator.hasNext()) {
                    AccessRightEntity currentRightEntity = accessRightEntityIterator.next();
                    //if role md type = null and one of access rights mdtype = null, than ok
                    if (metadataTypeId == null) {
                        if (currentRightEntity.getMetadataTypeEntity1() == null || currentRightEntity.getMetadataTypeEntity2() == null) {
                            continue;
                        } else {
                            accessRightEntityIterator.remove();
                        }
                    } else {
                        if (currentRightEntity.getMetadataTypeEntity1() == null && currentRightEntity.getMetadataTypeEntity2() == null) {
                            accessRightEntityIterator.remove();
                        } else {
                            if (currentRightEntity.getMetadataTypeEntity1() != null) {
                                //exist
                                if (metadataTypeId.equalsIgnoreCase(currentRightEntity.getMetadataTypeEntity1().getId())) {
                                    continue;
                                }
                            }
                            if (currentRightEntity.getMetadataTypeEntity2() != null) {
                                //exist
                                if (metadataTypeId.equalsIgnoreCase(currentRightEntity.getMetadataTypeEntity2().getId())) {
                                    continue;
                                }
                            }
                            accessRightEntityIterator.remove();
                        }
                    }
                }
            }
        }
    }

    private static void mergeEntitlemets(String mdType1, String mdType2, Set<? extends AbstractMembershipXrefEntity> entities) {
        if (entities == null) {
            return;
        }
        Iterator<? extends AbstractMembershipXrefEntity> iterator = entities.iterator();
        while (iterator.hasNext()) {
            AbstractMembershipXrefEntity entity = iterator.next();
            AbstractMetdataTypeEntity entity1 = null;
            AbstractMetdataTypeEntity entity2 = null;

            if (entity.getMemberEntity() instanceof AbstractMetdataTypeEntity) {
                entity1 = (AbstractMetdataTypeEntity) entity.getMemberEntity();
            }
            if (entity.getEntity() instanceof AbstractMetdataTypeEntity) {
                entity2 = (AbstractMetdataTypeEntity) entity.getEntity();
            }
            if (entity1 == null || entity2 == null) {
                continue;
            }

            String entityMetadataTypeId1 = entity1.getType() == null ? null : entity1.getType().getId();
            String entityMetadataTypeId2 = entity2.getType() == null ? null : entity2.getType().getId();

            if (StringUtils.isBlank(mdType1) && StringUtils.isBlank(mdType2)) {
                if (StringUtils.isNotBlank(entityMetadataTypeId1) && StringUtils.isNotBlank(entityMetadataTypeId2)) {
                    iterator.remove();
                } else if (StringUtils.isBlank(entityMetadataTypeId1) && StringUtils.isNotBlank(entityMetadataTypeId2)) {
                    iterator.remove();
                } else if (StringUtils.isNotBlank(entityMetadataTypeId1) && StringUtils.isBlank(entityMetadataTypeId2)) {
                    iterator.remove();
                }
            } else if (StringUtils.isNotBlank(mdType1) && StringUtils.isNotBlank(mdType2)) {
                if (StringUtils.isBlank(entityMetadataTypeId1) && StringUtils.isBlank(entityMetadataTypeId2)) {
                    iterator.remove();
                } else if (StringUtils.isBlank(entityMetadataTypeId1) && StringUtils.isNotBlank(entityMetadataTypeId2)) {
                    iterator.remove();
                } else if (StringUtils.isNotBlank(entityMetadataTypeId1) && StringUtils.isBlank(entityMetadataTypeId2)) {
                    iterator.remove();
                } else if (StringUtils.isNotBlank(entityMetadataTypeId1) && StringUtils.isNotBlank(entityMetadataTypeId2)) {
                    if (!(StringUtils.equals(entityMetadataTypeId1, mdType1) && StringUtils.equals(entityMetadataTypeId2, mdType2)) &&
                            !(StringUtils.equals(entityMetadataTypeId1, mdType1) && StringUtils.equals(entityMetadataTypeId2, mdType2))) {
                        iterator.remove();
                    }
                }
            } else if (StringUtils.isNotBlank(mdType1) && StringUtils.isBlank(mdType2)) {
                if (!StringUtils.isBlank(entityMetadataTypeId1) || !StringUtils.isBlank(entityMetadataTypeId2)) {
                    if (StringUtils.isNotBlank(entityMetadataTypeId1) && StringUtils.isNotBlank(entityMetadataTypeId2)) {
                        iterator.remove();
                    } else if (StringUtils.isBlank(entityMetadataTypeId1) && StringUtils.isNotBlank(entityMetadataTypeId2)) {
                        if (!mdType1.equals(entityMetadataTypeId2)) {
                            iterator.remove();
                        }
                    } else if (StringUtils.isNotBlank(entityMetadataTypeId1) && StringUtils.isBlank(entityMetadataTypeId2)) {
                        if (!mdType1.equals(entityMetadataTypeId1)) {
                            iterator.remove();
                        }
                    }
                } else {
                    iterator.remove();
                }
            } else if (StringUtils.isBlank(mdType1) && StringUtils.isNotBlank(mdType2)) {
                if (StringUtils.isBlank(entityMetadataTypeId1) && StringUtils.isBlank(entityMetadataTypeId2)) {
                    iterator.remove();
                } else if (StringUtils.isNotBlank(entityMetadataTypeId1) && StringUtils.isNotBlank(entityMetadataTypeId2)) {
                    iterator.remove();
                } else if (StringUtils.isBlank(entityMetadataTypeId1) && StringUtils.isNotBlank(entityMetadataTypeId2)) {
                    if (!mdType2.equals(entityMetadataTypeId2)) {
                        iterator.remove();
                    }
                } else if (StringUtils.isNotBlank(entityMetadataTypeId1) && StringUtils.isBlank(entityMetadataTypeId2)) {
                    if (!mdType2.equals(entityMetadataTypeId1)) {
                        iterator.remove();
                    }
                }
            }
        }

    }

    public static void mergeAccessRights(AccessRightEntity accessRightEntity) {

        if (accessRightEntity == null)
            return;

        String mdType1 = accessRightEntity.getMetadataTypeEntity1() == null ? null : accessRightEntity.getMetadataTypeEntity1().getId();
        String mdType2 = accessRightEntity.getMetadataTypeEntity2() == null ? null : accessRightEntity.getMetadataTypeEntity2().getId();

        mergeEntitlemets(mdType1, mdType2, accessRightEntity.getResource2ResourceMappings());
        mergeEntitlemets(mdType1, mdType2, accessRightEntity.getRole2ResourceMappings());
        mergeEntitlemets(mdType1, mdType2, accessRightEntity.getRole2RoleMappings());
    }

}
